"""
A Python module for controlling power and brightness
of the official Raspberry Pi 7" touch display.

Ships with a CLI, GUI and Python API.

:Author: Linus Groh
:License: MIT license
"""

import os
import sys
import time
from typing import Any

__author__ = "Linus Groh"
__version__ = "1.8.1"
PATH = "/sys/class/backlight/rpi_backlight/"


def _perm_denied() -> None:
    print(
        "A permission error occured. You must either run this program as root or change the"
    )
    print("permissions for the backlight access as described on the GitHub page.")
    sys.exit()


def _get_value(name: str) -> str:
    try:
        with open(os.path.join(PATH, name), "r") as f:
            return f.read()
    except (OSError, IOError) as err:
        if err.errno == 13:
            _perm_denied()


def _set_value(name: str, value: Any) -> None:
    try:
        with open(os.path.join(PATH, name), "w") as f:
            f.write(str(value))
    except (OSError, IOError) as err:
        if err.errno == 13:
            _perm_denied()


def get_actual_brightness() -> int:
    """Return the actual display brightness."""
    return int(_get_value("actual_brightness"))


def get_max_brightness() -> int:
    """Return the maximum display brightness."""
    return int(_get_value("max_brightness"))


def get_power() -> bool:
    """Return wether the display is powered on or not."""
    # 0 is on, 1 is off
    return not int(_get_value("bl_power"))


def set_brightness(value: int, smooth: bool = False, duration: float = 1) -> None:
    """Set the display brightness.

    :param value: Brightness value between 11 and 255
    :param smooth: Boolean if the brightness should be faded or not
    :param duration: Fading duration in seconds
    """

    max_value = get_max_brightness()
    if not isinstance(value, int):
        raise ValueError("integer required, got '{}'".format(type(value)))
    if not 10 < value <= max_value:
        raise ValueError(
            "value must be between 11 and {}, got {}".format(max_value, value)
        )

    if smooth:
        if not isinstance(duration, (int, float)):
            raise ValueError(
                "integer or float required, got '{}'".format(type(duration))
            )
        actual = get_actual_brightness()
        diff = abs(value - actual)
        while actual != value:
            actual = actual - 1 if actual > value else actual + 1

            _set_value("brightness", actual)
            time.sleep(duration / diff)
    else:
        _set_value("brightness", value)


def set_power(on: bool) -> None:
    """Set the display power on or off.

    :param on: Boolean whether the display should be powered on or not
    """

    # 0 is on, 1 is off
    _set_value("bl_power", int(not on))

if __name__=="__main__":

    print("Module not ready to be executed standalone")
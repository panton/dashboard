#!/usr/bin/env python3

import sys
import os
import pkgutil
import signal
from configparser import ConfigParser
import logging


from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication
from mainView import *

# Get dashboard path to add it into PYTHONPATH search list
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, os.path.dirname(os.path.dirname(dir_path)))

import dashboard.views.components
from dashboard.utils import rpi_blacklight
import importlib

class DashboardMainController(object):

    BACK_TO_DASHBOARD = 0
    SHOW_COMPONENT_MAIN_WIDGET = 1
    SHOW_COMPONENT_DASHBOARD_CONTEXT_MENU = 2
    DASHBOARD_DEFAULTVIEW_TIMEOUT = 120000 # Seconds

    MAX_GRID_COLUMNS = 3
    EMULATE_CONTEXT_BUTTON_TIMEOUT = 0.7
    CACHE_PATH = os.path.join(os.path.expanduser("~"), ".dashboard")
    CONFIG_PATH = os.path.join(CACHE_PATH, ".config")
    DEFAULT_LOGFILE_PATH = os.path.join(CACHE_PATH, "logfile.txt")
    DEFAULT_DASHBOARD_BRIGHTNESS = 25

    # Keys for config file configuration
    CONFIG_SECTION_NAME = 'Main'
    CONFIG_LOGFILE_PATH = 'logfile_path'
    CONFIG_DEFAULT_COMPONENT = 'default_component'
    CONFIG_DASHBOARD_COLUMNS = 'dashboard_columns'
    CONFIG_DASHBOARD_BRIGHTNESS = 'dashboard_brightness'

    def __init__(self, view):

        self.components = []
        self.view = view

        self.configure_config_file()

        logging.info('Init Main Controller with view resolution: ' + str(view.width()) + "x" + str(view.height()))

        # Read all classe in components folder
        package = dashboard.views.components
        prefix = package.__name__ + "."
        for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix):
            print("Found submodule %s (is a package: %s)" % (modname, ispkg))
            module =importlib.import_module(modname)

            component = module.get_main_class(self)
            if (component is not None):
                self.components += [component]


        # Calcule component dashboard size depending on max columns
        component_width = (view.width() // self.dashboard_columns)
        logging.info('Component size: ' + str(component_width) + "x" + str(component_width))

        column = 0
        row = 0
        for component in self.components:
            component_widget = component.get_dashboard_widget(component_width)
            view.gridLayout.addWidget(component_widget, row, column, 1, 1)

            if column >= self.dashboard_columns:
                row += 1
                column = 0
            else:
                column += 1

            # Transform default component string to a real component object if it exist
            if self.default_component is not None and self.default_component == component.__class__.__name__:
                self.default_component = component

        # Configure default component by default
        if self.components and self.default_component is None:
            self.default_component = self.components[0]

        self.default_view_timer = QTimer(self.view)
        self.default_view_timer.setInterval(DashboardMainController.DASHBOARD_DEFAULTVIEW_TIMEOUT)
        self.default_view_timer.timeout.connect(self.start_default_view_component)
        self.default_view_timer.start()

    def configure_config_file(self):
        # Check config file
        self.config = ConfigParser()
        found = self.config.read(DashboardMainController.CONFIG_PATH)

        # Create it if it doesnt exist
        if (len(found) == 0):
            print("Configuration file not found on " + DashboardMainController.CONFIG_PATH)

            self.config.add_section(DashboardMainController.CONFIG_SECTION_NAME)
            if not os.path.exists(os.path.dirname(DashboardMainController.CONFIG_PATH)):
                os.makedirs(os.path.dirname(DashboardMainController.CONFIG_PATH))

            with open(DashboardMainController.CONFIG_PATH, 'w') as configfile:
                self.config.write(configfile)

        # Read or create log file
        property_list = self.config[DashboardMainController.CONFIG_SECTION_NAME]
        if DashboardMainController.CONFIG_LOGFILE_PATH in property_list:
            self.logfile = property_list[DashboardMainController.CONFIG_LOGFILE_PATH]
        else:
            # Basic logging
            self.logfile = DashboardMainController.DEFAULT_LOGFILE_PATH

        self.config[DashboardMainController.CONFIG_SECTION_NAME][DashboardMainController.CONFIG_LOGFILE_PATH] = self.logfile

        logging.basicConfig(format="%(asctime)s [%(filename)s] %(levelname)s %(message)s",
                    filename=self.logfile, level=logging.INFO)

        # Take default component if any (as a String)
        if DashboardMainController.CONFIG_DEFAULT_COMPONENT in property_list:
            self.default_component = property_list[DashboardMainController.CONFIG_DEFAULT_COMPONENT]
        else:
            self.default_component = None

        # Read Colums, configuration
        if DashboardMainController.CONFIG_DASHBOARD_COLUMNS in property_list:
            self.dashboard_columns = int(property_list[DashboardMainController.CONFIG_DASHBOARD_COLUMNS])
        else:
            # Set default values
            self.dashboard_columns = DashboardMainController.MAX_GRID_COLUMNS
            self.config[DashboardMainController.CONFIG_SECTION_NAME][DashboardMainController.CONFIG_DASHBOARD_COLUMNS] = str(self.dashboard_columns)

        # Read brighness information
        if DashboardMainController.CONFIG_DASHBOARD_BRIGHTNESS in property_list:
            self.dashboard_brighness = int(property_list[DashboardMainController.CONFIG_DASHBOARD_BRIGHTNESS])
        else:
            # Set default values
            self.dashboard_brighness = DashboardMainController.DEFAULT_DASHBOARD_BRIGHTNESS
            self.config[DashboardMainController.CONFIG_SECTION_NAME][DashboardMainController.CONFIG_DASHBOARD_BRIGHTNESS] = str(self.dashboard_brighness)

        # Set brightness
        try:
            rpi_blacklight.set_brightness(self.dashboard_brighness)
        except Exception as e:
            logging.error('Blacklight control not available')

    def save_config_file(self):
        print("Saving config file")
        for component in self.components:
            component.update_config_file()

        with open(DashboardMainController.CONFIG_PATH, 'w') as configfile:
            self.config.write(configfile)

    def send_message(self, message, component=None, param=None):
        if (message is DashboardMainController.BACK_TO_DASHBOARD):
            self.view.central_widget.insertWidget(0, self.view.gridLayoutWidget)
            self.view.central_widget.setCurrentIndex(0)

            # Restart timer to control default view
            print('Restarting default control timer')
            self.default_view_timer.start()

        elif (message is DashboardMainController.SHOW_COMPONENT_MAIN_WIDGET):
            component_widget = component.get_main_widget()
            self.view.central_widget.addWidget(component_widget)
            self.view.central_widget.setCurrentWidget(component_widget)
            # Stop timer to control default view
            self.default_view_timer.stop()
        elif (message is DashboardMainController.SHOW_COMPONENT_DASHBOARD_CONTEXT_MENU):
            component_context_menu = component.get_dashboard_widget_context_menu()

            # Check that Exit action has not been added yet
            already_added = False
            for action in component_context_menu.actions():
                if action.text() == "Exit":
                    already_added = True

            if not already_added:
                component_context_menu.addSeparator()
                component_context_menu.addAction( "Set as default", lambda: self.configure_default_view(component))
                component_context_menu.addAction("Exit", self.exit)

            component_context_menu.exec_(param.globalPos())
            # Restart timer to control default view
            print('Restarting default control timer')
            self.default_view_timer.start()

    def exit(self):

        # Stopping timer if needed
        self.default_view_timer.stop()

        # Saving config of each component
        self.save_config_file()

        self.view.close()
        QApplication.quit()

    def configure_default_view(self, component):
        self.default_component = component
        self.config[DashboardMainController.CONFIG_SECTION_NAME][DashboardMainController.CONFIG_DEFAULT_COMPONENT] = component.__class__.__name__

    def start_default_view_component(self):
        self.send_message(DashboardMainController.SHOW_COMPONENT_MAIN_WIDGET,self.default_component)

dashboard_instance = None

def sigint_handler(signum, frame):
    print('You pressed Ctrl+C!')
    dashboard_instance.exit()


# Start main application
if __name__=="__main__":

    app = QApplication(sys.argv)

    timer = QTimer()
    timer.start(500)  # You may change this if you wish.
    timer.timeout.connect(lambda: None)  # Let the interpreter run each 500 ms.

    view = Ui_MainWindow()
    dashboard_instance = DashboardMainController(view)
    view.set_controler(dashboard_instance)
    signal.signal(signal.SIGINT, sigint_handler)

    #view.show()
    view.showFullScreen()
    sys.exit(app.exec_())

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainView.ui'
#
# Created by: PyQt5 UI code generator 5.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QWidget, QHBoxLayout


class Ui_MainWindow(QWidget):

    def __init__(self):
        super().__init__()
        self.setObjectName("MainWindow")

        self.main_layout = QHBoxLayout()
        self.main_layout.setContentsMargins(0,0,0,0)
        self.setLayout(self.main_layout)

        # Create the central QStackedWidget widget
        self.central_widget = QtWidgets.QStackedWidget()
        self.central_widget.setObjectName("centralwidget")

        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSpacing(20)
        self.gridLayout.setObjectName("gridLayout")

        self.gridLayoutWidget = QtWidgets.QWidget()
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayoutWidget.setLayout(self.gridLayout)
        self.gridLayoutWidget.setStyleSheet("background: gray")

        self.central_widget.addWidget(self.gridLayoutWidget)
        self.central_widget.setCurrentWidget(self.gridLayoutWidget)

        self.main_layout.addWidget(self.central_widget)

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self)

    def set_controler(self, controler):
        self.dashboard_instance = controler

    def closeEvent(self, event):
        print("Goodbay")
        self.dashboard_instance.exit()

    def retranslateUi(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("MainWindow", "MainWindow"))
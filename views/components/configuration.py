import logging
import os
import time

from PyQt5 import QtWidgets
from PyQt5.QtGui import QPixmap, QIcon
from PyQt5.QtWidgets import QMenu, QSizePolicy, QVBoxLayout, QHBoxLayout, QLabel, QSlider, QCheckBox
from PyQt5.QtCore import Qt, QSize

from dashboard.utils import rpi_blacklight
from dashboard.views.components import component
from dashboard.views.main_controler import DashboardMainController


def get_main_class(main_controler):
    return Configuration(main_controler)

class Configuration(component.Component):
    """This class implement Component interface to  describe a Configuration component for the dashboard"""
    CONFIG_SECTION_NAME = 'Configuration'

    def __init__(self, main_controler):
        self.main_controler = main_controler

        self.dashboard_widget = None
        self.main_widget = None
        self.dashboard_widget_context_menu = None
        self.configuration_widget = None

    def get_dashboard_widget(self, size: int):
        if (self.dashboard_widget is None):
            logging.info("Creating dashboard widget")
            self.dashboard_widget = ConfigurationDashboardWidget(self, size)

        return self.dashboard_widget

    def get_dashboard_widget_context_menu(self):
        if (self.dashboard_widget_context_menu is None):
            self.dashboard_widget_context_menu = QMenu()

        return self.dashboard_widget_context_menu

    def get_main_widget(self):
        if (self.main_widget is None):
            logging.info('Creating main widget')
            self.main_widget = ConfigurationMainWidget(self.main_controler)

        return self.main_widget

    def update_config_file(self):
        pass

    def get_configuration_widget(self):
        if (self.configuration_widget is None):
            logging.info('Creating configuration widget')

            self.configuration_widget = ConfigurationConfigurationWidget(self.main_controler)

        return self.configuration_widget

class ConfigurationDashboardWidget(QtWidgets.QLabel):

    # Source dir is on ../../
    script_path = os.path.realpath(__file__)
    ICON_PATH = os.path.abspath(os.path.join(script_path, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.join(ICON_PATH, 'icons/Configuration-icon.png')

    def __init__(self, component: component.Component, size: int):
        super().__init__()

        self.recommended_size = size
        self.setObjectName("ConfigurationButton")

        policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setSizePolicy(policy)
        self.label_pixmap = QPixmap(ConfigurationDashboardWidget.ICON_PATH)

        self.setPixmap(self.label_pixmap.scaled(size, size, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.setStyleSheet("border: 3px solid black;border-radius: 20px;")
        self.main_component = component

    def mousePressEvent(self, event):
        super().mousePressEvent(event)

        self.setStyleSheet("border: 1px solid black;border-radius: 20px;background-color: rgba(255, 255, 255, 20);")

        self.press_time = time.time()
        self.press_event = event

    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)

        pressed_time = time.time() - self.press_time
        self.setStyleSheet("border: 3px solid black;border-radius: 20px;")

        if pressed_time > DashboardMainController.EMULATE_CONTEXT_BUTTON_TIMEOUT:
            self.main_component.main_controler.send_message(DashboardMainController.SHOW_COMPONENT_DASHBOARD_CONTEXT_MENU, component=self.main_component, param=self.press_event)
        else:
            self.main_component.main_controler.send_message(DashboardMainController.SHOW_COMPONENT_MAIN_WIDGET, component=self.main_component)


class ConfigurationMainWidget(QtWidgets.QWidget):

    # Source dir is on ../../
    script_path = os.path.realpath(__file__)
    ICON_PATH = os.path.abspath(os.path.join(script_path, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.join(ICON_PATH, 'icons/Back-icon.png')

    def __init__(self, controler: DashboardMainController):
        super().__init__()

        self.main_controler = controler

        self.setObjectName("ConfigurationMainWidget")
        self.main_layout = QVBoxLayout()

        # Create the navigator button to back to dashboard
        self.control_bar_widget = QtWidgets.QWidget()
        self.control_bar_layout = QHBoxLayout()

        self.back_button = QtWidgets.QPushButton()
        self.back_button.clicked.connect(self.back_to_dashboard)
        self.back_button.setIcon(QIcon(QPixmap(ConfigurationMainWidget.ICON_PATH)))

        self.control_bar_layout.addWidget(self.back_button)
        self.control_bar_layout.addStretch(4)
        self.control_bar_widget.setLayout(self.control_bar_layout)

        # Create the central QTabWidget widget
        self.central_widget_tab = QtWidgets.QTabWidget()
        self.central_widget_tab.setObjectName("ConfigurationMainWidget:central_widget_tab")

        for component in controler.components:
            component_configuration_widget = component.get_configuration_widget()

            if component_configuration_widget:
                self.central_widget_tab.addTab(component_configuration_widget, component_configuration_widget.get_tab_name())

        self.main_layout.addWidget(self.control_bar_widget)
        self.main_layout.addWidget(self.central_widget_tab)
        self.setLayout(self.main_layout)

    def back_to_dashboard(self):
        self.main_controler.send_message(DashboardMainController.BACK_TO_DASHBOARD)

class ConfigurationConfigurationWidget(QtWidgets.QWidget):

    TAB_NAME = "General configuration"

    def __init__(self, controler: DashboardMainController):
        super().__init__()

        self.setObjectName("ConfigurationConfigurationWidget")

        self.main_controler = controler

        # Read saved properties with folder list configuration
        config_sections = self.main_controler.config.sections()

        # Create the progreess luminosity configuration bar
        self.brightness_configuration_widget = ConfigurationBrightnessWidget(self)

        # Create layout and add widgets
        self.main_layout = QHBoxLayout()
        self.main_layout.addWidget(self.brightness_configuration_widget)

        self.setLayout(self.main_layout)

    def get_tab_name(self):
        return ConfigurationConfigurationWidget.TAB_NAME

class ConfigurationBrightnessWidget(QtWidgets.QWidget):

    def __init__(self, main_configuration):
        super().__init__()

        self.setObjectName("ConfigurationBrightnessWidget")

        self.main_configuration = main_configuration

        # Create the Label
        self.label = QLabel("Brightness")
        self.label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        # Create the Slider
        self.brightness_slider = QSlider(Qt.Horizontal)
        self.brightness_slider.setMinimum(0)
        self.brightness_slider.setMaximum(255)

        try:
            self.brightness_slider.setValue(rpi_blackligh.get_actual_brightness())
        except Exception as e:
            logging.error('Blacklight control not available, set default value')
            self.brightness_slider.setValue(20)

        self.brightness_slider.setTickPosition(QSlider.TicksBelow)
        self.brightness_slider.setTickInterval(5)
        self.brightness_slider.valueChanged.connect(self.slider_change)

        # Create the Checkbox to set it automatically
        self.automatic_checkbox = QCheckBox("Auto")
        self.automatic_checkbox.setChecked(False)
        self.automatic_checkbox.stateChanged.connect(self.automatic_checkbox_state)

        # Create layout and add widgets
        self.main_layout = QHBoxLayout()
        self.main_layout.addWidget(self.label)
        self.main_layout.addWidget(self.brightness_slider)
        self.main_layout.addWidget(self.automatic_checkbox)

        self.setLayout(self.main_layout)

    def slider_change(self, value):
        logging.info("Brighness change to " + str(value))

        # Unselect auto check box if it is selected
        if self.automatic_checkbox.isChecked():
            self.automatic_checkbox.setChecked(False)

        # Set brightness
        try:
            rpi_blacklight.set_brightness(value)
        except Exception as e:
            logging.error('Blacklight control not available')

    def automatic_checkbox_state(self,b):

        if self.automatic_checkbox.isChecked():
            logging.info("Automatic checkbox is selected ")
        else:
            logging.info("Automatic checkbox is unselected ")



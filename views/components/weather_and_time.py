import logging
import os

from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer, QTime, Qt, QSize
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMenu, QLCDNumber, QSizePolicy

from dashboard.views.components import component
from dashboard.sensors.rpi_DS18B20_temp_sensor import RpiDS18B20TempSensor
from dashboard.views.main_controler import DashboardMainController

import time

def get_main_class(main_controler):
    return WeatherComponent(main_controler)

class WeatherComponent(component.Component):
    """This class implement Component interface to  describe a weather component element for
    the dashboard"""
    def __init__(self, main_controler):
        self.dashboard_widget_context_menu = None
        self.dashboard_widget = None
        self.main_widget = None
        self.temperature = 'Undefined'

        try:
            self.main_controler = main_controler
            sensor = RpiDS18B20TempSensor()
            self.temperature = str(sensor.get_value())
        except Exception as e:
            print('Sensor not available')

    def get_dashboard_widget(self, size: int):
        if (self.dashboard_widget is None):
            logging.info("Creating dashboard widget")
            self.dashboard_widget = WeatherDashboardWidget(self, size)

        return self.dashboard_widget

    def get_main_widget(self):
        if (self.main_widget is None):
            self.main_widget = DigitalClock(self.main_controler)
            self.main_widget.setObjectName("Weather")

            self.main_widget.mouseReleaseEvent = self.main_widget_dispmessage

        self.main_widget.setCursor(Qt.BlankCursor)

        return self.main_widget

    def get_dashboard_widget_context_menu(self):
        if (self.dashboard_widget_context_menu is None):
            self.dashboard_widget_context_menu = QMenu()

        return self.dashboard_widget_context_menu

    def main_widget_dispmessage(self, event):
        self.main_controler.send_message(DashboardMainController.BACK_TO_DASHBOARD)

    def update_config_file(self):
        pass

    def get_configuration_widget(self):
        pass

class WeatherDashboardWidget(QtWidgets.QLabel):

    # Source dir is on ../../
    script_path = os.path.realpath(__file__)
    ICON_PATH = os.path.abspath(os.path.join(script_path, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.join(ICON_PATH, 'icons/Weather-icon.png')

    def __init__(self, component: component.Component, size: int):
        super().__init__()

        self.recommended_size = size
        self.setObjectName("WeatherButton")

        policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setSizePolicy(policy)
        self.label_pixmap = QPixmap(WeatherDashboardWidget.ICON_PATH)

        self.setPixmap(self.label_pixmap.scaled(size, size, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.setStyleSheet("border: 3px solid black;border-radius: 20px;")
        self.main_component = component

        logging.info("Create dashboard widget with size: " + str(self.width()) + "x" + str(self.height()))

    def mousePressEvent(self, event):
        super().mousePressEvent(event)

        self.setStyleSheet("border: 1px solid black;border-radius: 20px;background-color: rgba(255, 255, 255, 20);")

        self.press_time = time.time()
        self.press_event = event

    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)

        pressed_time = time.time() - self.press_time
        self.setStyleSheet("border: 3px solid black;border-radius: 20px;")

        if pressed_time > DashboardMainController.EMULATE_CONTEXT_BUTTON_TIMEOUT:
            self.main_component.main_controler.send_message(DashboardMainController.SHOW_COMPONENT_DASHBOARD_CONTEXT_MENU, component=self.main_component, param=self.press_event)
        else:
            self.main_component.main_controler.send_message(DashboardMainController.SHOW_COMPONENT_MAIN_WIDGET, component=self.main_component)

class DigitalClock(QLCDNumber):
    def __init__(self, controler):
        super(DigitalClock, self).__init__(controler.view)

        # get the palette
        palette = self.palette()

        # foreground color
        palette.setColor(palette.WindowText, Qt.darkGray)
        # background color
        palette.setColor(palette.Background, Qt.black)
        # "light" border
        palette.setColor(palette.Light, Qt.black)
        # "dark" border
        palette.setColor(palette.Dark, Qt.black)

        self.setAutoFillBackground(True)

        # set the palette
        self.setPalette(palette)
        self.setSegmentStyle(QLCDNumber.Filled)

        timer = QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)

        self.showTime()

    def showTime(self):
        time = QTime.currentTime()
        text = time.toString('hh:mm')
        if (time.second() % 2) == 0:
            text = text[:2] + ' ' + text[3:]

        self.display(text)

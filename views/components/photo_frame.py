import json
import logging
import os
import random
import time
from enum import Enum

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMenu, QFileDialog, QAbstractItemView, QListView, QTreeView, QSizePolicy, QHBoxLayout, \
    QLabel, QButtonGroup, QRadioButton, QComboBox, QVBoxLayout

from dashboard.views.components import component
from dashboard.views.main_controler import DashboardMainController


def get_main_class(main_controler):
    return PhotoFrame(main_controler)

class PhotoFrame(component.Component):
    """This class implement Component interface to  describe a Digital Photo Frame component for the dashboard"""
    CONFIG_SECTION_NAME = 'PhotoFrame'
    CONFIG_FOLDER_LIST = 'folder_list'
    CONFIG_DISPLAY_MODE = 'display_mode'
    CONFIG_INTERVAL_DURATION = 'interval_duration'

    CONFIG_DEFAULT_FOLDER = os.path.join(os.path.expanduser("~"), 'Downloads')
    CONFIG_DEFAULT_INTERVAL_DURATION = 5000.0

    class DisplayMode(Enum):
        SEQUENTIAL = "Sequential"
        RANDOM = "Random"

        @staticmethod
        def from_str(label):
            if label == 'Sequential':
                return PhotoFrame.DisplayMode.SEQUENTIAL
            elif label == 'Random':
                return PhotoFrame.DisplayMode.RANDOM
            else:
                raise NotImplementedError

    def __init__(self, main_controler):
        self.TAG = self.__class__.__name__
        self.main_controler = main_controler

        self.dashboard_widget = None
        self.main_widget = None
        self.configuration_widget = None
        self.dashboard_widget_context_menu = None
        self.file_dialog = None
        self.file_list = []

        # Read saved properties with folder list configuration
        config_sections = main_controler.config.sections()
        if not PhotoFrame.CONFIG_SECTION_NAME in config_sections:
            main_controler.config.add_section(PhotoFrame.CONFIG_SECTION_NAME)
            self.folder_list = [PhotoFrame.CONFIG_DEFAULT_FOLDER]
            self.display_mode = PhotoFrame.DisplayMode.SEQUENTIAL
            self.timer_interval = PhotoFrame.CONFIG_DEFAULT_INTERVAL_DURATION
        else:
            # Get folder list from config file
            property_list = main_controler.config[PhotoFrame.CONFIG_SECTION_NAME]
            if PhotoFrame.CONFIG_FOLDER_LIST in property_list:
                folder_list_property = main_controler.config[PhotoFrame.CONFIG_SECTION_NAME][PhotoFrame.CONFIG_FOLDER_LIST]
                self.folder_list = json.loads(folder_list_property)
            else:
               self.folder_list = [PhotoFrame.CONFIG_DEFAULT_FOLDER]

            # Get display mode from config file
            if PhotoFrame.CONFIG_DISPLAY_MODE in property_list:
                self.display_mode = PhotoFrame.DisplayMode.from_str(main_controler.config[PhotoFrame.CONFIG_SECTION_NAME][PhotoFrame.CONFIG_DISPLAY_MODE])
            else:
                self.display_mode = PhotoFrame.DisplayMode.SEQUENTIAL

            # Get interval duration from config file
            if PhotoFrame.CONFIG_INTERVAL_DURATION in property_list:
                self.timer_interval = float(main_controler.config[PhotoFrame.CONFIG_SECTION_NAME][PhotoFrame.CONFIG_INTERVAL_DURATION])
            else:
                self.timer_interval = PhotoFrame.CONFIG_DEFAULT_INTERVAL_DURATION

        self.update_file_list()

    def update_file_list(self):

        # walk in folder list to obtain all pictures files
        for folder in self.folder_list:
            count = 0
            for root, dirs, files in os.walk(folder):
                for file in files:
                    if file.lower().endswith(".jpg"):
                        self.file_list += [(os.path.join(root, file))]
                        count += 1
            print(self.TAG + " added " + str(count) + " files from " + folder)

        # Select a random number between 0 and file list length
        self.current_file_index = 0
        if self.file_list:
            self.current_file_index = random.randint(1, len(self.file_list)) -1

    def get_dashboard_widget(self, size: int):
        if (self.dashboard_widget is None):
            logging.info("Creating dashboard widget")
            self.dashboard_widget = PhotoFrameDashboardWidget(self, size)

        return self.dashboard_widget

    def select_action_handler(self):

        if (self.file_dialog is None):
            self.file_dialog = QFileDialog()
            self.file_dialog.setFileMode(QFileDialog.DirectoryOnly)
            self.file_dialog.setOption(QFileDialog.DontUseNativeDialog,True)

            list_view = self.file_dialog.findChild(QTreeView)
            if list_view is not None:
                list_view.setSelectionMode(QAbstractItemView.MultiSelection)

            tree_view = self.file_dialog.findChild(QListView)
            if tree_view is not None:
                tree_view.setSelectionMode(QAbstractItemView.MultiSelection)

        self.file_dialog.exec_()

        folder_list = self.file_dialog.selectedFiles()
        if folder_list is not []:
            self.folder_list = folder_list
            self.update_file_list()

    def get_dashboard_widget_context_menu(self):
        if (self.dashboard_widget_context_menu is None):
            self.dashboard_widget_context_menu = QMenu()
            self.dashboard_widget_context_menu.addAction("Select folders", self.select_action_handler )

        return self.dashboard_widget_context_menu

    def get_main_widget(self):
        if (self.main_widget is None):
            self.main_widget = QtWidgets.QLabel(self.main_controler.view)
            self.main_widget.setAlignment(QtCore.Qt.AlignCenter)
            self.main_widget.mouseReleaseEvent = self.main_widget_dispmessage
            self.main_widget.resize(self.main_controler.view.width(), self.main_controler.view.height())

            self.main_widget.setAutoFillBackground(True)
            palette = self.main_widget.palette()
            palette.setColor(self.main_widget.backgroundRole(), QtCore.Qt.black)
            self.main_widget.setPalette(palette)

            self.main_widget.setObjectName("PhotoFrame")
            self.timer_handler()

            self.timer = QtCore.QTimer(self.main_widget)
            self.timer.setInterval(self.timer_interval)
            self.timer.timeout.connect(self.timer_handler)
        else:
            print(self.TAG + " main widget already created")

        self.main_widget.setCursor(Qt.BlankCursor)

        return self.main_widget

    def update_config_file(self):
        self.main_controler.config[PhotoFrame.CONFIG_SECTION_NAME][PhotoFrame.CONFIG_FOLDER_LIST] = json.dumps(self.folder_list)
        self.main_controler.config[PhotoFrame.CONFIG_SECTION_NAME][PhotoFrame.CONFIG_DISPLAY_MODE] = self.display_mode.value
        self.main_controler.config[PhotoFrame.CONFIG_SECTION_NAME][PhotoFrame.CONFIG_INTERVAL_DURATION] = str(self.timer_interval)

    def timer_handler(self):

        if self.file_list:
            # Change main widget to next file
            pixmap = QtGui.QPixmap(self.file_list[self.current_file_index])

            label_height = self.main_widget.height();
            label_width = self.main_widget.width();

            self.main_widget.setPixmap(pixmap.scaled(label_width, label_height, QtCore.Qt.KeepAspectRatio))

            # Calculate next index
            if self.display_mode is PhotoFrame.DisplayMode.SEQUENTIAL:
                self.current_file_index += 1

                if self.current_file_index >= len(self.file_list):
                    self.current_file_index = 0
            else:
                aux = random.randint(0,len(self.file_list) - 1)
                if aux == self.current_file_index:
                    self.current_file_index += 1

                    if self.current_file_index >= len(self.file_list):
                        self.current_file_index = 0
                else:
                    self.current_file_index = random.randint(0,len(self.file_list) - 1)

    def main_widget_dispmessage(self, event):
        self.timer.stop()
        self.main_controler.send_message(DashboardMainController.BACK_TO_DASHBOARD)

    def get_configuration_widget(self):
        if (self.configuration_widget is None):
            logging.info('Creating configuration widget')

            self.configuration_widget = PhotoFrameConfigurationWidget(self)

        return self.configuration_widget

class PhotoFrameDashboardWidget(QtWidgets.QLabel):

    # Source dir is on ../../
    script_path = os.path.realpath(__file__)
    ICON_PATH = os.path.abspath(os.path.join(script_path, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.abspath(os.path.join(ICON_PATH, os.pardir))
    ICON_PATH = os.path.join(ICON_PATH, 'icons/Photo-icon.png')

    def __init__(self, component: component.Component, size: int):
        super().__init__()

        self.recommended_size = size
        self.setObjectName("PhotoFrameDashboardWidget")

        policy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.setSizePolicy(policy)
        self.label_pixmap = QPixmap(PhotoFrameDashboardWidget.ICON_PATH)

        self.setPixmap(self.label_pixmap.scaled(size, size, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.setStyleSheet("border: 3px solid black;border-radius: 20px;")
        self.main_component = component

    def mousePressEvent(self, event):
        super().mousePressEvent(event)

        self.setStyleSheet("border: 1px solid black;border-radius: 20px;background-color: rgba(255, 255, 255, 20);")

        self.press_time = time.time()
        self.press_event = event

    def mouseReleaseEvent(self, event):
        super().mouseReleaseEvent(event)

        pressed_time = time.time() - self.press_time
        self.setStyleSheet("border: 3px solid black;border-radius: 20px;")

        if pressed_time > DashboardMainController.EMULATE_CONTEXT_BUTTON_TIMEOUT:
            self.main_component.main_controler.send_message(DashboardMainController.SHOW_COMPONENT_DASHBOARD_CONTEXT_MENU, component=self.main_component, param=self.press_event)
        else:
            self.main_component.main_controler.send_message(DashboardMainController.SHOW_COMPONENT_MAIN_WIDGET, component=self.main_component)
            self.main_component.timer.setInterval(self.main_component.timer_interval)
            self.main_component.timer.start() # it starts the scheduler

class PhotoFrameConfigurationWidget(QtWidgets.QWidget):

    TAB_NAME = "Photo Frame"
    def __init__(self, component: component.Component):
        super().__init__()

        self.setObjectName("PhotoFrameConfigurationWidget")
        self.main_component = component

        # Create the Label
        self.display_mode_label = QLabel("Display Mode")
        self.display_mode_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        # Create the radio buttons
        self.display_mode_button_group_widget = QtWidgets.QWidget()
        self.display_mode_button_group_layout = QHBoxLayout()
        self.display_mode_button_group = QButtonGroup(self.display_mode_button_group_widget)
        self.display_mode_random_radio_button = QRadioButton("Random")
        self.display_mode_sequential_radio_button = QRadioButton("Sequential")

        self.display_mode_button_group.addButton(self.display_mode_random_radio_button)
        self.display_mode_button_group.addButton(self.display_mode_sequential_radio_button)
        self.display_mode_button_group_layout.addWidget(self.display_mode_random_radio_button)
        self.display_mode_button_group_layout.addWidget(self.display_mode_sequential_radio_button)
        self.display_mode_button_group_widget.setLayout(self.display_mode_button_group_layout)

        # Connect ButtonGroup to clicked callback
        self.display_mode_button_group.buttonClicked.connect(self.display_mode_change)

        # Configure the layout
        self.display_mode_layout = QHBoxLayout()
        self.display_mode_layout.addWidget(self.display_mode_label)
        self.display_mode_layout.addWidget(self.display_mode_button_group_widget)

        #
        # Interval duration configuration
        #
        self.duration_label = QLabel("Duration")
        self.duration_label.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

        self.duration_box = QComboBox(self)
        for i in range(5,21):
            self.duration_box.addItem(str(i))

        self.duration_layout = QHBoxLayout()
        self.duration_layout.addWidget(self.duration_label)
        self.duration_layout.addWidget(self.duration_box)

        # Create layout and add widgets
        self.main_layout = QVBoxLayout()
        self.main_layout.addLayout(self.duration_layout)
        self.main_layout.addLayout(self.display_mode_layout)

        self.setLayout(self.main_layout)
        self.update_configuration()

    def update_configuration(self):

        if self.main_component.display_mode is PhotoFrame.DisplayMode.RANDOM:
            self.display_mode_random_radio_button.setChecked(True)
        else:
            self.display_mode_sequential_radio_button.setChecked(True)

    def display_mode_change(self, button):

        if button is self.display_mode_random_radio_button:
            self.main_component.display_mode = PhotoFrame.DisplayMode.RANDOM
        else:
            self.main_component.display_mode = PhotoFrame.DisplayMode.SEQUENTIAL


    def get_tab_name(self):
        return PhotoFrameConfigurationWidget.TAB_NAME
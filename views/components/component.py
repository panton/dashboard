from PyQt5.QtWidgets import QWidget


def get_main_class(main_controler):
    pass

class Component(object):
    """This class is the basic interface to any Dashboard Component """

    def get_dashboard_widget(self, size: int) -> QWidget:
        raise NotImplementedError()

    def get_main_widget(self) -> QWidget:
        raise NotImplementedError()

    def update_config_file(self) -> None:
        raise NotImplementedError()

    def get_dashboard_widget_context_menu(self) -> QWidget:
        raise NotImplementedError()

    def get_configuration_widget(self) -> QWidget:
        raise NotImplementedError()

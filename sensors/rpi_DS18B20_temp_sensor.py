from dashboard.sensors.temp_sensor import TempSensor

import os
import glob
import time

class RpiDS18B20TempSensor(TempSensor):
    """This class is the basic interface to any Temperature Sensor """

    DEVICE_DIR = '/sys/bus/w1/devices/'
    def __init__(self):
        #TODO Check modproble w1-gpio and w1-therm
        self.device_folder = glob.glob(RpiDS18B20TempSensor.DEVICE_DIR + '28*')[0]
        self.device_file = self.device_folder + '/w1_slave'
        self.scale_unit = TempSensor.SCALE_UNIT_CELSIUS

    def get_value(self):

        if (self.scale_unit == TempSensor.SCALE_UNIT_CELSIUS):
            return self.__read_temp_c()
        else:
            return self.__read_temp_f()

    def __read_temp_raw(self):
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def __read_temp_c(self):
        lines = self.__read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.__read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = int(temp_string) / 1000.0 # TEMP_STRING IS THE SENSOR OUTPUT, MAKE SURE IT'S AN INTEGER TO DO THE MATH
            temp_c = str(round(temp_c, 1)) # ROUND THE RESULT TO 1 PLACE AFTER THE DECIMAL, THEN CONVERT IT TO A STRING
            return temp_c

    def __read_temp_f(self):
        lines = self.__read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.__read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_f = (int(temp_string) / 1000.0) * 9.0 / 5.0 + 32.0 # TEMP_STRING IS THE SENSOR OUTPUT, MAKE SURE IT'S AN INTEGER TO DO THE MATH
            temp_f = str(round(temp_f, 1)) # ROUND THE RESULT TO 1 PLACE AFTER THE DECIMAL, THEN CONVERT IT TO A STRING
            return temp_f

if __name__ == '__main__':

    sensor = RpiDS18B20TempSensor()
    print("Sensor value is: " + str(sensor.get_value()))
from dashboard.sensors.sensor import Sensor


class TempSensor(Sensor):
    """This class is the basic interface to any Temperature Sensor """

    SCALE_UNIT_CELSIUS = 0
    SCALE_UNIT_FAHRENHEIT = 1

    def configure_scale_unit(self, value):
        self.scale_unit = value

    def get_value(self):
        pass
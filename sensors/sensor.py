class Sensor(object):
    """This class is the basic interface to any Sensor """

    def get_value(self):
        pass